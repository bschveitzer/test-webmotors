import React from 'react';
import { Cart as ShoppingCart } from '../../features/cart/Cart';

export function Cart() {
  return (
    <div>
      <ShoppingCart/>
    </div>
  );
}