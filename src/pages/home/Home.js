import React from 'react';
import { Showcase } from '../../features/showcase/Showcase'

export function Home() {
  return (
    <div>
      <Showcase />
    </div>
  );
}