import React from 'react';
import './App.css';
import { BaseRouter } from "./app/routes";
import { Header } from "./features/header/Header";
import {
  BrowserRouter as Router,
} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router>
        <Header/>
        <div className="App__top">
          <BaseRouter/>
        </div>
      </Router>
    </div>
  );
}

export default App;
