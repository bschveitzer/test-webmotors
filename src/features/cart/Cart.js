import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import {
  selectCartItems
} from './Cart.store';
// import localforage from 'localforage'
import { CartItem } from './components/CartItem';
import styles from './Cart.module.css';

export function Cart() {
  const items = useSelector(selectCartItems);
  const [amount, setAmount] = useState(0);

// const getInitialItems = async () => {
//   try {
//     const items = await localforage.getItem('cartItems')
//     console.log('tei', items);
//   } catch (err) {
//     console.error(err);
//   }
// }

  // useEffect(getInitialItems())

  return (
    <div className={ styles.container }>
      <p className={ styles.cart__label }>Meu carrinho</p>
      <div className={ styles.row }>
        { items.length > 0 && items.map( (item, index) => (
          <CartItem item={ item }/>
        ))}
        { items.length === 0 && <span className={ styles.cart__label}>Sem itens no carrinho.</span>}
      </div>
      { items.length > 0 && <p className={ styles.cart__label}>Total do pedido: R$ { amount }</p> }
    </div>
  );
}
