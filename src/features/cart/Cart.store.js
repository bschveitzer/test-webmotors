import { createSlice } from '@reduxjs/toolkit';
import localforage from 'localforage'

export const cartSlice = createSlice({
  name: 'cart',
  initialState: {
    cartItems: [],
  },
  reducers: {
    setItems: (state, action) => {
      state.cartItems = action.payload;
    },
    addItem: (state, action) => {
      delete action.payload.sizes;
      let isPresent = false;

      if(state.cartItems.length > 0) {
        state.cartItems.map( item => {
          if(item.sku === action.payload.sku) {
            item.quantity += 1;
            isPresent = true;
          }
        })
      } 

      if(!isPresent) {
        action.payload.quantity = 1;
        action.payload.number_price = Number(action.payload.actual_price.match(/[\d\.]+/));
        state.cartItems.push(action.payload);

      }

      localforage.setItem('cartItems', JSON.parse(JSON.stringify(state.cartItems)));
    }
  },
});

export const { setItems, addItem } = cartSlice.actions;

export const selectCartItems = state => state.cart.cartItems;

export default cartSlice.reducer;
