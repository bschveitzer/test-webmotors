import React, { useState } from 'react';
import styles from './CartItem.module.css';
import classNames from 'classnames';

export function CartItem(props) {
  const [item, setItem] = useState(props.item);

  return (
    <div className={ classNames(styles.card, styles.row) }>
      <div className={styles.row}>
        <div className={styles.card__media}>
          {item.image && <img src={item.image} alt=""/>}
        </div>
      </div>
      <div className={styles.card__info}>
        <div className={styles.card__info_title}>{item.name}</div>
        <div className={styles.card__info_label}>Tamanho: {item.size}</div>
        <p className={styles.card__info_label}>R${item.quantity * item.number_price}</p>
      </div>
    </div>
  );
}
