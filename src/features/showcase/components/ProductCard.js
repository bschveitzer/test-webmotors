import React, { useState } from 'react';
import styles from './ProductCard.module.css';
import classNames from 'classnames';

export function ProductCard(props) {
  const [item, setItem] = useState(props.item);
  const [selectedItem, setSelectedItem] = useState({});

  const addItem = () => {
    props.onAddClick(selectedItem);
    setSelectedItem({});
  }

  return (
    <div className={styles.card}>
      <div className={ classNames(styles.row, styles.card__top)}>
        <div className={styles.card__media}>
          {item.image && <img src={item.image} alt=""/>}
        </div>
        {item.discount_percentage && <span className={styles.card__badge}>-{item.discount_percentage}</span>}
      </div>
      <div className={styles.card__info}>
        <div className={styles.card__info_title}>{item.name}</div>
        <div className={styles.card__info_section}>
          <p className={styles.card__info_label}>Selecione tamanho:</p>
          {item.sizes.map((size, index) => (
            <span
              className={classNames(styles.card__info_sizes, { [styles.disabled]: !size.available, [styles.selected]: size.sku === selectedItem.sku })}
              onClick={() => setSelectedItem({ ...item, ...size })}>
              {size.size}
            </span>
          ))}
        </div>
        <div className={styles.card__info_section}>
          {item.discount_percentage && <p className={styles.card__info_label_old}>{item.regular_price}</p>}
          <p className={styles.card__info_label}>{item.actual_price}</p>
        </div>
        <button 
          className={classNames(styles.button, { [styles.button__disabled]: selectedItem.style !== item.style })} 
          disabled={selectedItem.style !== item.style}
          onClick={ () => addItem(selectedItem)}>
            Adicionar
        </button>
      </div>
    </div>
  );
}
