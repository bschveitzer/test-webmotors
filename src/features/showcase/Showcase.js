import React from 'react';
import { useSelector, useDispatch } from 'react-redux';

import {
  selectItems
} from './Showcase.store';

import {
  addItem
} from '../cart/Cart.store'

import { ProductCard } from './components/ProductCard';
import styles from './Showcase.module.css';

export function Showcase() {
  const items = useSelector(selectItems);
  const dispatch = useDispatch();

  return (
    <div>
      <div className={styles.row}>
        { items.map( (item, index) => (
          <ProductCard item={ item } onAddClick={ (selectedItem) =>dispatch(addItem(selectedItem)) }/>
        )) }
      </div>
    </div>
  );
}
