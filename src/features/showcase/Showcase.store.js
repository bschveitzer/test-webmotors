import { createSlice } from '@reduxjs/toolkit';
import db from '../../db.json'

export const showcaseSlice = createSlice({
  name: 'showcase',
  initialState: {
    items: db.products,
  },
  reducers: {
    setItems: (state, action) => {
      state.items = action.payload;
    },
  },
});

export const { setItems } = showcaseSlice.actions;

export const selectItems = state => state.showcase.items;

export default showcaseSlice.reducer;
