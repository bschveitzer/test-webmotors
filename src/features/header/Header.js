import React from 'react';
import { useSelector } from 'react-redux';
import {
  selectCartItems
} from '../cart/Cart.store';
import {
  Link
} from "react-router-dom";
import styles from './Header.module.css';
import { BiCart } from "react-icons/bi";

export function Header() {
  const items = useSelector(selectCartItems);

  return ( 
    <header className={styles.header}>
      <div className={styles.header__media}>
        Teste Projeto 22
      </div>
      <div className={ styles.header__actions}>
        <Link to="/cart">
          <div className={ styles.header__actions_badge }>{ items.length }</div>
          <BiCart size="1.3em" className={ styles.header__actions_icons } color="#fff"/>
        </Link>
      </div>
    </header> 
  );
}
