import React from "react";
import {
  Switch,
  Route,
} from "react-router-dom";

import { Home } from "../pages/home/Home";
import { Cart } from "../pages/cart/Cart"

export function BaseRouter() {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/cart">
        <Cart />
      </Route>
    </Switch>
  );
}