import { configureStore } from '@reduxjs/toolkit';
import showCasereducer from '../features/showcase/Showcase.store';
import cartReducer from '../features/cart/Cart.store'

export default configureStore({
  reducer: {
    showcase: showCasereducer,
    cart: cartReducer
  },
});
