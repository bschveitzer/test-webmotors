O projeto foi iniciado pelo React create app, com template do react router unido. A ideia foi mostrar uma vitrine de produtos, com a opção da inserção desses produtos em um carrinho fictício. Para essa solução foram utilizadas as seguintes principais ferramentas:
 -React
 -React Router
 -Redux

 A estrutura escolhida foi para modular as funcionalidades, sendo organizadas em pastas desacopladas, utilizando o Redux para principal interação entre os componentes.

 Para a instalação do projeto, basta rodar `npm install` ou `yarn`.

 PS1: Gostaria de ter atomizado mais ainda os componentes, criando componentes base para uso replicado.
 PS2: A estrutura de teste está inexistente, porém gostaria de ter usado o jest para resolver os casos.

## Scripts disponíveis

### `yarn start`

Rodar o app em dev mode.<br />
Abrir [http://localhost:3000](http://localhost:3000).

Vai recarregar sempre que tiver alguma alteração e mostrar erros no console.
### `yarn test`
### `yarn build`